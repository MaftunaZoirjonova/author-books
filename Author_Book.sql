CREATE TABLE Authors (
    ID int NOT NULL,
    Name varchar(255) NOT NULL,
    Birth_Date date NOT NULL,
    Country varchar(255) NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE Books (
    ID int NOT NULL,
    Title varchar(255) NOT NULL,
    Description varchar(255),
    Genre varchar(255) NOT NULL,
    Number_Of_Pages int NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE Author_Book (
    Author_ID int,
    Book_ID int,
    CONSTRAINT FK_AUTHOR FOREIGN KEY (Author_ID) REFERENCES Authors(ID),
    CONSTRAINT FK_BOOK FOREIGN KEY (Book_ID) REFERENCES Books(ID)
);